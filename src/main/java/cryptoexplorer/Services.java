/*
 * Copyright 2009-2012 Marcelo Morales
 *
 * Licensed under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for
 * the specific language governing permissions and limitations under the
 * License. under the License.
 */

package cryptoexplorer;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.Provider;
import java.security.Security;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeSet;

import static com.google.common.collect.Sets.newTreeSet;
import static java.io.File.separator;
import static java.lang.String.format;
import static java.lang.System.getProperty;
import static java.lang.System.getenv;
import static java.security.Provider.Service;
import static java.security.Security.addProvider;
import static java.security.Security.getProviders;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

@Path("/services")
@Produces(APPLICATION_JSON)
public class Services {

    @POST
    @Path("/loadbc")
    public String loadBC() {
        addProvider(new BouncyCastleProvider());
        return "ok";
    }

    @POST
    @Path("/loadff")
    public String loadPK() throws WebApplicationException {
        String osname = getProperty("os.name");
        if (osname == null) {
            throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity("os not found").build());
        }

        String profiledir = null;
        // TODO: NSS x Windows

        if (osname.toLowerCase().contains("windows")) {
            profiledir = getenv("APPDATA") + separator + "Mozilla" + separator + "Firefox" + separator + "Profiles";
        }
        if (osname.toLowerCase().contains("linux")) {
            profiledir = getenv("HOME") + separator + ".mozilla" + separator + "firefox";
        }
        if (profiledir == null) {
            throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity("os not supported").build());
        }

        File file = new File(profiledir);
        if (!file.exists() || !file.isDirectory()) {
            throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity("dir not found").build());
        }

        File[] files = file.listFiles();
        if (files == null) {
            throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity("dir empty").build());
        }

        for (File defaultProfileDir : files) {
            if (!defaultProfileDir.isDirectory()) {
                continue;
            }
            if (defaultProfileDir.getName().toLowerCase().contains("default")) {
                try {
                    String tmpConfig =
                            "name=JSS\n" +
                                    "description=FFPKCS11\n" +
                                    "nssLibraryDirectory=/usr/lib/x86_64-linux-gnu/nss/\n" +
                                    "nssSecmodDirectory = " + defaultProfileDir.getPath() + "\n" +
                                    "nssDbMode = readOnly\n" +
                                    "attributes = compatibility";

                    ByteArrayInputStream strConfig = new ByteArrayInputStream(tmpConfig.getBytes());

                    Constructor c = Class.forName("sun.security.pkcs11.SunPKCS11").getConstructor(new Class[]{InputStream.class});
                    Provider nss = (Provider) c.newInstance(strConfig);
                    Security.addProvider(nss);
                    return "ok";
                } catch (NoSuchMethodException e) {
                    throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity(e).build());
                } catch (ClassNotFoundException e) {
                    throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity(e).build());
                } catch (InstantiationException e) {
                    throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity(e).build());
                } catch (IllegalAccessException e) {
                    throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity(e).build());
                } catch (InvocationTargetException e) {
                    throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity(e).build());
                }
            }
        }

        throw new WebApplicationException(Response.status(INTERNAL_SERVER_ERROR).entity("not found").build());
    }

    @GET
    @Path("/list")
    public String servicesList() {

        ImmutableSet<String> hasDetail = ImmutableSet.of("KeyStore", "SecureRandom");

        TreeSet<Map<String, String>> elements = newMapOrderedBy("name");

        for (Provider provider : getProviders()) {
            for (Service service : provider.getServices()) {
                String type = service.getType();
                elements.add(ImmutableMap.of(
                        "name", type,
                        "description", type.replaceAll(format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"), " "),
                        "hasDetail", hasDetail.contains(type)?type:"false"));
            }
        }

        return new Gson().toJson(elements);
    }

    @GET
    @Path("/describe/{service}")
    public String describe(@PathParam("service") String service) {

        TreeSet<Map<String, String>> elements = newMapOrderedBy("provider");

        for (Provider provider : getProviders()) {
            for (Service providerService : provider.getServices()) {
                if (providerService.getType().equals(service)) {
                    elements.add(ImmutableMap.of("algorithm", providerService.getAlgorithm(), "provider",
                            provider.getName()));
                }
            }
        }

        return new Gson().toJson(elements);
    }

    private static TreeSet<Map<String, String>> newMapOrderedBy(final String sortKey) {
        return newTreeSet(new Comparator<Map<String, String>>() {

            @Override
            public int compare(Map<String, String> o1, Map<String, String> o2) {
                return o1.get(sortKey).compareTo(o2.get(sortKey));
            }
        });
    }
}