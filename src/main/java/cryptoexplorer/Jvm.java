package cryptoexplorer;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import java.util.*;

import static com.google.common.collect.Iterators.*;
import static com.google.common.collect.Lists.newArrayList;
import static java.util.Arrays.asList;
import static java.util.Collections.sort;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * @author Marcelo Morales
 *         Date: 8/24/12
 */
@Path("/jvm")
@Produces(APPLICATION_JSON)
public class Jvm {

    @POST
    @Path("/req")
    public String request(@Context final HttpServletRequest rq) {
        @SuppressWarnings("unchecked")
        Iterator<String> attributes = transform((Iterator<String>) forEnumeration(rq.getAttributeNames()),
                new Function<String, String>() {

                    @Override
                    public String apply(String input) {
                        Object attribute = rq.getAttribute(input);
                        if (attribute instanceof String) {
                            return input + "=" + attribute;
                        }

                        if (attribute != null) {
                            return input + " (class " + attribute.getClass() + ")=" + attribute.toString();
                        }

                        return input + " is null";
                    }
                });

        @SuppressWarnings("unchecked")
        Iterator<String> parameters = transform((Iterator<String>) forEnumeration(rq.getParameterNames()),
                new Function<String, String>() {

                    @Override
                    public String apply(String input) {
                        return input + " (param) = " + asList(rq.getParameterValues(input));
                    }
                });


        @SuppressWarnings("unchecked")
        Iterator<String> headers = transform((Iterator<String>) forEnumeration(rq.getHeaderNames()),
                new Function<String, String>() {

                    @Override
                    public String apply(String input) {
                        return input + " (header) = " + rq.getHeader(input);
                    }
                });


        ArrayList<String> src = newArrayList(concat(attributes, parameters, headers));

        sort(src);

        return new Gson().toJson(src);
    }

    @GET
    @SuppressWarnings("unchecked")
    @Path("/jvm")
    public String properties() {
        Properties properties = System.getProperties();
        List<Object> transform = newArrayList(Collections2.transform(properties.entrySet(), new Function<Object, Object>() {

            @Override
            public Object apply(Object input) {
                Map.Entry<String, String> entry = (Map.Entry<String, String>) input;

                return ImmutableMap.of("key", entry.getKey(), "value", entry.getValue());
            }
        }));

        sort(transform, new Comparator<Object>() {

            @Override
            public int compare(Object o1, Object o2) {
                Map<String, String> u1 = (Map<String, String>) o1;
                Map<String, String> u2 = (Map<String, String>) o2;
                return u1.get("key").compareTo(u2.get("key"));
            }
        });

        return new Gson().toJson(transform);
    }
}
