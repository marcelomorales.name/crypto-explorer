/*
 * Copyright 2009-2012 Marcelo Morales
 *
 * Licensed under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0 Unless required by
 * applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the License for
 * the specific language governing permissions and limitations under the
 * License. under the License.
 */

package cryptoexplorer;

import org.junit.Ignore;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.security.KeyStore;

/**
 * @author Marcelo Morales
 *         Date: 8/28/12
 */
public class FirefoxTest {

    @Test
    @Ignore("Works on linux, luego de hackear un poquito")
    public void testFirefoxKeyStore() throws Exception {
        String profiledir = "/home/marcelo/.mozilla/firefox/cayv3nai.default/";
        String tmpConfig =
                "name=JSS\n" +
                        "description=JSS PKCS11\n" +
                        "nssLibraryDirectory=/usr/lib/x86_64-linux-gnu/nss/\n" +
                        "nssSecmodDirectory = " + profiledir + "\n" +
                        "nssDbMode = readOnly\n" +
                        "nssModule = keystore\n" +
                        "attributes = compatibility";

        ByteArrayInputStream strConfig = new ByteArrayInputStream(tmpConfig.getBytes());

        Constructor c = Class.forName("sun.security.pkcs11.SunPKCS11").getConstructor(new Class[]{InputStream.class});
        java.security.Provider nss = (java.security.Provider) c.newInstance(strConfig);
        KeyStore keystore = KeyStore.getInstance("PKCS11", nss);
        keystore.load(null, null);
        System.out.println(keystore.size());
    }
}
